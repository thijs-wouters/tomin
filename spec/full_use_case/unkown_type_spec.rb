RSpec.describe 'Unknown type of exercise' do
  it 'raises an UnknownTypeError' do
    expect(WorksheetRepo).to be_empty

    form = RequestWorksheetForm.new(type: :unknown)
    use_case = RequestWorksheet.new(form, Child.new)

    expect { use_case.execute }.to raise_error(UnknownTypeError, 'I do not support exercises of unknown type') do |error|
      expect(error.type).to eq(:unknown)
    end
  end
end
