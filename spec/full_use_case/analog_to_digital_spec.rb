RSpec.describe 'Exercising converting analog to digital time' do
  it 'returns a list of analog to digital time exercises' do
    srand(123)
    expect(WorksheetRepo).to be_empty

    form = RequestWorksheetForm.new(type: :analog_to_digital, amount: 2)
    current_user = Child.new
    use_case = RequestWorksheet.new(form, current_user)

    use_case.execute

    expect(WorksheetRepo).not_to be_empty

    worksheet = WorksheetRepo.first

    expect(worksheet.type).to eq(:analog_to_digital)
    expect(worksheet.exercises.count).to eq(2)

    expect(worksheet.exercises[0]).to be_a(AnalogToDigitalExercise)
    expect(worksheet.exercises[0].analog).to eq(Tomin::AnalogClock.new(hours: 13, minutes: 0))
    expect(worksheet.exercises[0].solution).to be_nil

    expect(worksheet.exercises[1]).to be_a(AnalogToDigitalExercise)
    expect(worksheet.exercises[1].analog).to eq(Tomin::AnalogClock.new(hours: 2, minutes: 0))
    expect(worksheet.exercises[1].solution).to be_nil
  end
end
