RSpec.describe 'Register new child' do
  let(:use_case) { RegisterChild.new(form) }

  before do
    expect(ChildRepo).to be_empty
    expect(ParentRepo).to be_empty
  end

  context 'with valid data' do
    let(:form) { RegisterChildForm.new(email: 'child@example.com', name: 'Test Testington', parent_email: 'parent@example.com', password: 'test') }

    it 'creates the child' do
      use_case.execute

      expect(ChildRepo).not_to be_empty
      child = ChildRepo.first
      expect(child.email).to eq('child@example.com')
      expect(child.name).to eq('Test Testington')
      expect(child.password).to eq('test')
    end

    shared_examples 'adds the child to the parent' do
      it 'adds the child to the parent' do
        use_case.execute

        expect(ParentRepo).not_to be_empty
        expect(ChildRepo).not_to be_empty

        parent = ParentRepo.first
        child = ChildRepo.first

        expect(child.parent).to be(parent)
      end
    end

    context 'the parent does not exist' do
      it 'creates the parent' do
        use_case.execute

        expect(ParentRepo).not_to be_empty
        parent = ParentRepo.first
        expect(parent.email).to eq('parent@example.com')
      end

      include_examples 'adds the child to the parent'
    end

    context 'the parent already exists' do
      let!(:existing_parent) { Parent.create(email: 'parent@example.com') }

      it 'uses the existing parent' do
        expect(ParentRepo).not_to be_empty
        use_case.execute

        expect(ParentRepo.count).to eq(1)
        expect(ChildRepo).not_to be_empty
        expect(ChildRepo.first.parent).to be(existing_parent)
      end

      include_examples 'adds the child to the parent'
    end
  end

  context 'with invalid data' do
    let(:form) { RegisterChildForm.new }

    it 'does not create the child' do
      expect { use_case.execute }.to raise_error(ValidationError)

      expect(ChildRepo).to be_empty
    end
  end
end
