RSpec.describe 'Exercising the tables of division' do
  describe 'requesting a new worksheet' do
    it 'returns a list of random division exercises' do
      srand(123)
      expect(WorksheetRepo).to be_empty

      form = RequestWorksheetForm.new(type: :tables_of_division, amount: 2)
      use_case = RequestWorksheet.new(form, Child.new)
      use_case.execute

      expect(WorksheetRepo).not_to be_empty

      tables_of_division = WorksheetRepo.first

      expect(tables_of_division.type).to eq(:tables_of_division)

      expect(tables_of_division.exercises.length).to eq(2)

      expect(tables_of_division.exercises[0]).to be_a(DivisionExercise)
      expect(tables_of_division.exercises[0].left_hand_side).to eq(6)
      expect(tables_of_division.exercises[0].right_hand_side).to eq(3)
      expect(tables_of_division.exercises[0].solution).to be_nil

      expect(tables_of_division.exercises[1]).to be_a(DivisionExercise)
      expect(tables_of_division.exercises[1].left_hand_side).to eq(7)
      expect(tables_of_division.exercises[1].right_hand_side).to eq(7)
      expect(tables_of_division.exercises[1].solution).to be_nil
    end
  end
end
