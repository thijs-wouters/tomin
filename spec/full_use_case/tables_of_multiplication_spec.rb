describe 'Exercising the tables of multiplication' do
  context 'when requesting a new worksheet' do
    it 'returns a list of random multiplication exercises' do
      srand(123)
      expect(WorksheetRepo).to be_empty

      form = RequestWorksheetForm.new(type: :tables_of_multiplication, amount: 2)
      current_user = Child.new

      use_case = RequestWorksheet.new(form, current_user)

      use_case.execute

      expect(WorksheetRepo).not_to be_empty

      tables_of_multiplication = WorksheetRepo.first

      expect(tables_of_multiplication.type).to eq(:tables_of_multiplication)

      expect(tables_of_multiplication.student).to be(current_user)
      expect(tables_of_multiplication.exercises.size).to be(2)

      expect(tables_of_multiplication.exercises[0]).to be_a(MultiplicationExercise)
      expect(tables_of_multiplication.exercises[0].left_hand_side).to eq(2)
      expect(tables_of_multiplication.exercises[0].right_hand_side).to eq(2)
      expect(tables_of_multiplication.exercises[0].solution).to be_nil

      expect(tables_of_multiplication.exercises[1]).to be_a(MultiplicationExercise)
      expect(tables_of_multiplication.exercises[1].left_hand_side).to eq(6)
      expect(tables_of_multiplication.exercises[1].right_hand_side).to eq(1)
      expect(tables_of_multiplication.exercises[1].solution).to be_nil
    end
  end

  context 'when submitting a worksheet' do
    before do
      WorksheetRepo.save(Worksheet.new(id: 123, exercises: [
        MultiplicationExercise.new(left_hand_side: 2, right_hand_side: 2),
        MultiplicationExercise.new(left_hand_side: 5, right_hand_side: 2),
        MultiplicationExercise.new(left_hand_side: 3, right_hand_side: 2),
      ]))
    end

    it 'sets the given solutions' do
      form = SubmitWorksheetForm.new(solutions: [
        "4",
        nil,
        "",
      ])

      use_case = SubmitWorksheet.new(form)

      use_case.execute(123)

      submitted_worksheet = WorksheetRepo.first

      expect(submitted_worksheet.exercises[0].solution).to eq(4)
      expect(submitted_worksheet.exercises[1].solution).to be_nil
      expect(submitted_worksheet.exercises[2].solution).to be_nil
    end

    it 'checks that the given solution is correct' do
      form = SubmitWorksheetForm.new(solutions: ["4", "9", "3"])

      use_case = SubmitWorksheet.new(form)

      use_case.execute(123)

      submitted_worksheet = WorksheetRepo.find(123)

      expect(submitted_worksheet.exercises[0]).to be_correct
      expect(submitted_worksheet.exercises[1]).not_to be_correct
      expect(submitted_worksheet.exercises[2]).not_to be_correct
    end

    it 'sets whether the exercises was solved or not' do
      form = SubmitWorksheetForm.new(solutions: ["4", nil, ""])

      use_case = SubmitWorksheet.new(form)

      use_case.execute(123)

      submitted_worksheet = WorksheetRepo.find(123)

      expect(submitted_worksheet.exercises[0]).to be_solved
      expect(submitted_worksheet.exercises[1]).not_to be_solved
      expect(submitted_worksheet.exercises[2]).not_to be_solved
    end
  end
end
