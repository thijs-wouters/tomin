RSpec.describe 'Time counting' do
  it 'returns time counting exercises' do
    srand(123)

    expect(WorksheetRepo).to be_empty

    form = RequestWorksheetForm.new(amount: 2, type: :time_counting)
    current_user = Child.new

    use_case = RequestWorksheet.new(form, current_user)

    use_case.execute

    expect(WorksheetRepo).not_to be_empty

    worksheet = WorksheetRepo.first

    expect(worksheet.type).to eq(:time_counting)

    expect(worksheet.exercises.length).to eq(2)

    expect(worksheet.exercises[0]).to be_a_kind_of(EndTimeExercise)
    expect(worksheet.exercises[0].start.hours).to eq(13)
    expect(worksheet.exercises[0].start.minutes).to eq(30)
    expect(worksheet.exercises[0].duration).to eq(45)
    expect(worksheet.exercises[0].solution).to be_nil

    expect(worksheet.exercises[1]).to be_a_kind_of(EndTimeExercise)
    expect(worksheet.exercises[1].start.hours).to eq(2)
    expect(worksheet.exercises[1].start.minutes).to eq(30)
    expect(worksheet.exercises[1].duration).to eq(30)
    expect(worksheet.exercises[1].solution).to be_nil
  end
end
