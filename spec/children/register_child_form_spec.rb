RSpec.describe RegisterChildForm do
  describe 'validations' do
    let(:valid_form) { RegisterChildForm.new(name: 'Test Testington', email: 'child@example.com', password: 'test', parent_email: 'parent@example.com') }

    [:name, :email, :password, :parent_email].each do |field|
      [nil, ''].each do |value|
        context "#{field.to_s}#{value.inspect}" do
          let(:form) { valid_form.tap { |form| form.send(to_writer(field), value) } }

          it_behaves_like 'an invalid form', field, :required
        end
      end
    end

    [:email, :parent_email].each do |email_field|
      context "#{email_field.to_s} fails email check" do
        let(:form) { valid_form.tap { |form| form.send(to_writer(email_field), 'test') } }

        it_behaves_like 'an invalid form', email_field, :invalid_email
      end

      context "#{email_field.to_s} passes email check" do
        let(:form) { valid_form.tap { |form| form.send(to_writer(email_field), 'test@example.com') } }

        it_behaves_like 'a valid form'
      end
    end

    context 'the email address already exists' do
      let(:form) { valid_form.tap { |form| form.email = 'child@example.com' } }

      before do
        Child.create(email: 'child@example.com')
      end

      it_behaves_like 'an invalid form', :email, :duplicate
    end

    context 'a valid form' do
      let(:form) { valid_form }

      it_behaves_like 'a valid form'
    end

    context 'multiple errors' do
      let(:form) { valid_form.tap { |form| form.email = '' && form.name = '' } }

      it 'has multiple errors' do
        expect { form.validate! }.to raise_error(ValidationError) do |validation_error|
          expect(validation_error.violations).to contain_exactly(Violated::Violation.new(:email, :required), Violated::Violation.new(:name, :required))
        end
      end
    end
  end
end
