RSpec.describe Child do
  describe '#authenticate' do
    subject { Child.new(password: 'test') }
    it 'returns true when the password matches the given one' do
      expect(subject.authenticate('test')).to be_truthy
    end

    it 'returns false when the password does not match' do
      expect(subject.authenticate('blep')).to be_falsey
    end
  end
end
