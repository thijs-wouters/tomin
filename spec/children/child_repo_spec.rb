RSpec.describe ChildRepo do
  describe '#with_email' do
    before do
      ChildRepo.backend.clear
    end

    context 'a child with the given email address exists' do
      let!(:expected_child) { Child.create(email: 'test@example.com') }

      it 'returns that child' do
        expect(ChildRepo.with_email('test@example.com')).to be(expected_child)
      end
    end

    context 'a child with the given email address does not exist' do
      it 'raises an UnknownEmailError' do
        expect { ChildRepo.with_email('unkown@example.com') }.to raise_error(ChildRepo::UnknownEmailError)
      end
    end
  end
end
