RSpec.shared_examples 'exercise' do |options|
  context 'when an incorrect solution is filled in' do
    before do
      subject.solve(options[:incorrect])
    end

    describe '#solved?' do
      it 'is true' do
        expect(subject.solved?).to be_truthy
      end
    end

    describe '#correct?' do
      it 'is false' do
        expect(subject.correct?).to be_falsey
      end
    end
  end

  context 'when a correct solution is filled in' do
    before do
      subject.solve(options[:correct])
    end

    describe '#solved?' do
      it 'is true' do
        expect(subject.solved?).to be_truthy
      end
    end

    describe '#correct?' do
      it 'is true' do
        expect(subject.correct?).to be_truthy
      end
    end
  end

  context 'when no solution is filled in' do
    before do
      subject.solve(nil)
    end

    describe '#solved?' do
      it 'is false' do
        expect(subject.solved?).to be_falsey
      end
    end

    describe '#correct?' do
      it 'is nil' do
        expect(subject.correct?).to be_nil
      end
    end
  end

  context 'when an empty solution is filled in' do
    before do
      subject.solve('')
    end

    describe '#solved?' do
      it 'is false' do
        expect(subject.solved?).to be_falsey
      end
    end

    describe '#correct?' do
      it 'is nil' do
        expect(subject.correct?).to be_nil
      end
    end
  end
end
