RSpec.shared_examples 'an invalid form' do |expected_field, expected_reason|
  it 'raises a validation error' do
    expect { form.validate! }.to raise_error(ValidationError) do |validation_error|
      if expected_field && expected_reason
        expect(validation_error.violations).not_to be_empty
        expect(validation_error.violations[0].field).to eq(expected_field)
        expect(validation_error.violations[0].reason).to eq(expected_reason)
      end
    end
  end
end

RSpec.shared_examples 'a valid form' do
  it 'does not raise a validation error' do
    expect { form.validate! }.not_to raise_error
  end
end
