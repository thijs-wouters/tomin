RSpec.describe EndTimeExercise do
  subject do
    EndTimeExercise.new(
      start: Tomin::Clock.new(hours: 0, minutes: 0),
      duration: 30,
    )
  end

  include_examples 'exercise',
                   correct: '0:30',
                   incorrect: '0:20'
end
