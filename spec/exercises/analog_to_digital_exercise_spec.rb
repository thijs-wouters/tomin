RSpec.describe AnalogToDigitalExercise do
  subject { AnalogToDigitalExercise.new(analog: Tomin::AnalogClock.new(hours: 13, minutes: 0)) }

  include_examples 'exercise',
                   correct: '13:00',
                   incorrect: '4:00'
end
