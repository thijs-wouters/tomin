require 'support/exercise'

RSpec.describe DivisionExercise do
  subject { DivisionExercise.new(left_hand_side: 6, right_hand_side: 3) }

  include_examples 'exercise', correct: '2', incorrect: '3'
end
