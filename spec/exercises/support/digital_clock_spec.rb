RSpec.describe Tomin::DigitalClock do
  describe '#==' do
    it 'returns true with a clock' do
      expect(Tomin::DigitalClock.new(hours: 1, minutes: 1) == Tomin::Clock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'returns true with a digital clock' do
      expect(Tomin::DigitalClock.new(hours: 1, minutes: 1) == Tomin::DigitalClock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'returns false with a analog clock' do
      expect(Tomin::DigitalClock.new(hours: 1, minutes: 1) == Tomin::AnalogClock.new(hours: 1, minutes: 1)).to be_falsey
    end
  end
end
