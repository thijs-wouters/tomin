RSpec.describe Tomin::AnalogClock do
  describe '#==' do
    it 'returns true with a clock' do
      expect(Tomin::AnalogClock.new(hours: 1, minutes: 1) == Tomin::Clock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'returns true with a analog clock' do
      expect(Tomin::AnalogClock.new(hours: 1, minutes: 1) == Tomin::AnalogClock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'returns false with a digital clock' do
      expect(Tomin::AnalogClock.new(hours: 1, minutes: 1) == Tomin::DigitalClock.new(hours: 1, minutes: 1)).to be_falsey
    end
  end
end
