RSpec.describe Tomin::Clock do
  describe '#+' do
    [
      [Tomin::Clock.new(hours: 0, minutes: 0), 1, Tomin::Clock.new(hours: 0, minutes: 1)],
      [Tomin::Clock.new(hours: 0, minutes: 0), 2, Tomin::Clock.new(hours: 0, minutes: 2)],
      [Tomin::Clock.new(hours: 0, minutes: 3), 2, Tomin::Clock.new(hours: 0, minutes: 5)],
      [Tomin::Clock.new(hours: 0, minutes: 59), 2, Tomin::Clock.new(hours: 1, minutes: 1)],
      [Tomin::Clock.new(hours: 1, minutes: 59), 2, Tomin::Clock.new(hours: 2, minutes: 1)],
      [Tomin::Clock.new(hours: 23, minutes: 59), 2, Tomin::Clock.new(hours: 0, minutes: 1)],
    ].each do |time, duration, result|
      context "#{time} + #{duration}" do
        it "equals #{result}" do
          expect(time + duration).to eq(result)
        end
      end
    end
  end

  describe '#==' do
    it 'is false when hours do not match' do
      expect(Tomin::Clock.new(hours: 1, minutes: 1) == Tomin::Clock.new(hours: 2, minutes: 1)).to be_falsey
    end

    it 'is false when minutes do not match' do
      expect(Tomin::Clock.new(hours: 1, minutes: 1) == Tomin::Clock.new(hours: 1, minutes: 2)).to be_falsey
    end

    it 'is true when both hours and minutes match' do
      expect(Tomin::Clock.new(hours: 1, minutes: 1) == Tomin::Clock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'is equal to an analog clock' do
      expect(Tomin::Clock.new(hours: 1, minutes: 1) == Tomin::AnalogClock.new(hours: 1, minutes: 1)).to be_truthy
    end

    it 'is equal to a digital clock' do
      expect(Tomin::Clock.new(hours: 1, minutes: 1) == Tomin::DigitalClock.new(hours: 1, minutes: 1)).to be_truthy
    end
  end

  describe '#to_s' do
    [
      [Tomin::Clock.new(hours: 12, minutes: 30), '12:30'],
      [Tomin::Clock.new(hours: 1, minutes: 30), '1:30'],
      [Tomin::Clock.new(hours: 1, minutes: 3), '1:03'],
    ].each do |time, result|
      context "#{time.inspect}.to_s" do
        it "returns #{result}" do
          expect(time.to_s).to eq(result)
        end
      end
    end
  end
end
