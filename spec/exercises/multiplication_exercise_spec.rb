require 'support/exercise'

RSpec.describe MultiplicationExercise do
  subject { MultiplicationExercise.new(left_hand_side: 4, right_hand_side: 3) }

  include_examples 'exercise', correct: '12', incorrect: '10'
end
