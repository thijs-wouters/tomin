RSpec.describe RequestWorksheetForm do
  describe 'defaults' do
    [
      [:time_counting, 2],
      [:tables_of_multiplication, 30],
      [:tables_of_division, 30],
    ].each do |type, expected_amount|
      context "when type = :#{type}" do
        subject { RequestWorksheetForm.new(type: type) }

        it "has default amount #{expected_amount}" do
          expect(subject.amount).to eq(expected_amount)
        end
      end
    end
  end
end
