RSpec.describe ParentRepo do
  describe '#with_email' do
    context 'a Parent exists with the email given' do
      it 'returns that parent' do
        parent = Parent.create(email: 'parent@example.com')
        expect(ParentRepo.with_email('parent@example.com')).to be(parent)
      end
    end

    context 'a Parent exists but not with the email given' do
      it 'raises an UnknownEmailError' do
        Parent.create(email: 'parent@example.com')
        expect { ParentRepo.with_email('other@example.com') }.to raise_error(ParentRepo::UnknownEmailError)
      end
    end
  end
end
