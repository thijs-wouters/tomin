require 'chassis'
require 'concord'
require 'violated'

require 'tomin/version'
require 'tomin/services'
require 'tomin/support'
require 'tomin/validations/email_validator'
require 'tomin/exercises'
require 'tomin/children'
require 'tomin/parents'
require 'tomin/worksheets'

Chassis.repo.use :memory
EncryptionService.use :fake
EmailValidationService.use :fake
