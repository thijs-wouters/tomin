class Child
  include Chassis::Persistence

  attr_accessor :email, :name, :password, :parent

  def authenticate(password_attempt)
    password == password_attempt
  end
end
