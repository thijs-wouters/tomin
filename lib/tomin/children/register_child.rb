class RegisterChild
  include Concord.new(:form)

  def execute
    form.validate!
    
    Child.create(email: form.email, name: form.name, password: form.password, parent: parent)
  end

  private
  def parent
    ParentRepo.with_email(form.parent_email)
  rescue ParentRepo::UnknownEmailError
    Parent.create(email: form.parent_email)
  end
end
