class RegisterChildForm
  include Chassis.form
  include Violated.new

  attribute :email, String
  attribute :name, String
  attribute :parent_email, String
  attribute :password, Password

  violates(:name).when_not :present
  violates(:email).when_not :present, :email, [:unique, ->(email) { ChildRepo.query(WithEmail.new(email)) }]
  violates(:parent_email).when_not :present, :email
  violates(:password).when_not :present

  def validate!
    raise ValidationError, violations unless valid?
  end
end
