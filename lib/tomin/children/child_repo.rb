class ChildRepo
  extend Chassis::Repo::Delegation

  UnknownEmailError = Tnt.boom

  def self.with_email(email)
    child = query(WithEmail.new(email))
    raise UnknownEmailError if child.nil?
    child
  end
end
