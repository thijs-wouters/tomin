class ParentRepo
  extend Chassis::Repo::Delegation

  UnknownEmailError = Tnt.boom

  def self.with_email(email)
    parent = query(WithEmail.new(email))
    raise UnknownEmailError.new if parent.nil?
    parent
  end
end
