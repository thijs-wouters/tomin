class ValidationError < StandardError
  attr_reader :violations

  def initialize(violations)
    @violations = violations
  end
end
