class Password < Virtus::Attribute
  def coerce(value)
    EncryptionService.encrypt(value)
  end
end
