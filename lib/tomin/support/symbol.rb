class Symbol
  def constantize
    to_s.split('_').collect(&:capitalize).join
  end
end
