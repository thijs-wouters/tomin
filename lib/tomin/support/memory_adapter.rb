class MemoryAdapter < Chassis::MemoryRepo
  def query_with_email(klass, query)
    all(klass).find do |entity|
      entity.email == query.email
    end
  end
end

Chassis.repo.register :memory, MemoryAdapter.new
