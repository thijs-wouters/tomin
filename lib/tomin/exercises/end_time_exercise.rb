class EndTimeExercise
  include(Tomin::Exercise.new(Tomin::Clock) do |exercise|
    exercise.start + exercise.duration
  end)

  attr_accessor :start, :duration
end
