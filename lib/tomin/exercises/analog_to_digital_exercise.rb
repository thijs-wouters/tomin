class AnalogToDigitalExercise
  include(Tomin::Exercise.new(Tomin::DigitalClock) do |exercise|
    exercise.analog.to_digital
  end)

  attr_accessor :analog
end
