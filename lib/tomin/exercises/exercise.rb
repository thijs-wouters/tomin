module Tomin
  class Exercise < Module
    def initialize(solution_class, &correct_solution)
      @module = Module.new do
        include Chassis::Initializable
        include solution_class::Coercer

        attr_accessor :solution

        def solved?
          !solution.nil?
        end

        def solve(solution)
          self.solution = coerce(solution) if solution && !solution.empty?
        end
      end

      @module.class_eval do
        define_method :correct? do
          solution == correct_solution.call(self) if solved?
        end
      end
    end

    def included(descendant)
      descendant.send(:include, @module)
    end
  end
end
