require_relative 'support/integer'

class MultiplicationExercise
  include(Tomin::Exercise.new(Integer) do |exercise|
    exercise.left_hand_side * exercise.right_hand_side
  end)

  attr_accessor :left_hand_side, :right_hand_side
end
