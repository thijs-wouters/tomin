module Tomin
  class AnalogClock
    extend Forwardable
    def_delegators :@clock, :hours, :hours=, :minutes, :minutes=, :to_digital

    def initialize(attr)
      @clock = Clock.new(attr)
    end

    def ==(other)
      correct_class?(other) && @clock == other
    end

    private
    def correct_class?(other)
      [AnalogClock, Clock].include?(other.class)
    end

    module Coercer
      include Clock::Coercer

      private
      def coerce(_)
        super.to_analog
      end
    end
  end
end
