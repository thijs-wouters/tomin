module Tomin
  class Clock
    include Chassis::Initializable

    attr_accessor :hours, :minutes

    def +(duration)
      Tomin::Clock.new(
        hours: add_hours(duration),
        minutes: add_minutes(duration))
    end

    def to_s
      "#{hours}:#{minutes.to_s.rjust(2, '0')}"
    end

    alias_method :eql?, :==

    def ==(other)
      other.hours == hours && other.minutes == minutes
    end

    def to_digital
      Tomin::DigitalClock.new(hours: hours, minutes: minutes)
    end

    def to_analog
      Tomin::AnalogClock.new(hours: hours, minutes: minutes)
    end

    private
    def add_hours(duration)
      (hours + (minutes + duration)/60) % 24
    end

    def add_minutes(duration)
      (minutes + duration) % 60
    end

    module Coercer
      private
      def coerce(string)
        Tomin::Clock.new(hours: string.split(':')[0].to_i, minutes: string.split(':')[1].to_i)
      end
    end
  end
end
