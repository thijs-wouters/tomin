module Tomin
  class DigitalClock
    extend Forwardable
    def_delegators :@clock, :hours, :hours=, :minutes, :minutes=

    def initialize(attr)
      @clock = Clock.new(attr)
    end

    def ==(other)
      correct_class?(other) && @clock == other
    end

    private
    def correct_class?(other)
      [DigitalClock, Clock].include?(other.class)
    end

    module Coercer
      include Clock::Coercer

      private
      def coerce(_)
        super.to_digital
      end
    end
  end
end
