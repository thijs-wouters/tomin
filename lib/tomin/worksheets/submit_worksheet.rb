class SubmitWorksheet
  include Concord.new(:form)

  def execute(worksheet_id)
    worksheet = WorksheetRepo.find(worksheet_id)
    worksheet.exercises.each_with_index do |exercise, index|
      exercise.solve(form.solutions[index])
    end
    worksheet.save
  end
end
