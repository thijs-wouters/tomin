class UnknownTypeError < StandardError
  attr_reader :type

  def initialize(type)
    super("I do not support exercises of #{type} type")
    @type = type
  end
end
