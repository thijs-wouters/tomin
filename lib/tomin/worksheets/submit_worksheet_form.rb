class SubmitWorksheetForm
  include Chassis.form

  attribute :solutions, Array[String]
end
