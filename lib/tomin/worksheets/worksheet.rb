class Worksheet
  include Chassis::Initializable
  include Chassis::Persistence

  attr_accessor :exercises, :student, :type
end
