require_relative 'generators'

class RequestWorksheet
  include Concord.new(:form, :current_user)

  def execute
    Worksheet.create do |worksheet|
      worksheet.student = current_user
      worksheet.exercises = generator.generate
      worksheet.type = form.type
    end
  end

  private
  def generator
    Kernel.const_get("#{form.type.constantize}Generator").new(form.amount)
  rescue NameError
    raise UnknownTypeError.new(form.type)
  end
end
