class RequestWorksheetForm
  include Chassis.form

  attribute :type, Symbol
  attribute :amount, Integer, default: :default_amount

  private
  def default_amount
    case type
      when :time_counting
        2
      else
        30
    end
  end
end
