require_relative 'generators/tables_of_multiplication_generator'
require_relative 'generators/tables_of_division_generator'
require_relative 'generators/time_counting_generator'
require_relative 'generators/analog_to_digital_generator'
