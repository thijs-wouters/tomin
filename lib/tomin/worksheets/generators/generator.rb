class Generator < Module
  def initialize(new_exercise)
    @instance_methods = Module.new

    @instance_methods.class_eval do
      define_method :initialize do |amount|
        instance_variable_set('@amount', amount)
      end

      define_method :generate do
        (1..@amount).map do
          new_exercise.call
        end
      end
    end
  end

  def included(desc)
    desc.send(:include, @instance_methods)
  end
end
