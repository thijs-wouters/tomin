require_relative 'generator'

class TimeCountingGenerator
  include Generator.new(lambda do
    EndTimeExercise.new(
      start: Tomin::Clock.new(hours: rand(23), minutes: [0, 15, 30, 45].sample),
      duration: [15, 30, 45, 60].sample,
    )
  end)
end
