require_relative 'generator'

class TablesOfDivisionGenerator
  include Generator.new(lambda do
    first = rand(9) + 1
    second = rand(10)
    DivisionExercise.new(left_hand_side: first*second, right_hand_side: first)
  end)
end
