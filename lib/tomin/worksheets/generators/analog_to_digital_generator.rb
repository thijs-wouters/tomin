require_relative 'generator'

class AnalogToDigitalGenerator
  include Generator.new(lambda { AnalogToDigitalExercise.new(analog: Tomin::AnalogClock.new(hours: rand(23), minutes: 0)) })
end
