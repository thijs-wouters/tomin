require_relative 'generator'

class TablesOfMultiplicationGenerator
  include Generator.new(lambda { MultiplicationExercise.new(left_hand_side: rand(10), right_hand_side: rand(10)) })
end
