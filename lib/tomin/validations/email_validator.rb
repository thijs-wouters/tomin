require_relative 'services/email_validation_service'
require 'violated/registry'
require 'violated/violation'

class EmailValidator
  attr_reader :field

  def initialize(field)
    @field = field
  end

  def validate(object)
    if object.send(@field) && !object.send(field).empty?
      object.violations << Violated::Violation.new(field, reason) unless valid?(object)
    end
  end

  def reason
    :invalid_email
  end

  def valid?(value)
    return true if value.nil? || value.empty?
    EmailValidationService.valid?(value)
  end
end

Violated::Registry.register :email, EmailValidator
