class FakeEmailValidationService
  def valid?(email)
    email.include?('@')
  end
end

EmailValidationService.register :fake, FakeEmailValidationService.new
