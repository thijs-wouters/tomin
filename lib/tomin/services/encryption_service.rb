class EncryptionService
  extend Interchange.new(:encrypt)
end

require_relative 'encryption_service/fake_encryption_service'
require_relative 'encryption_service/bcrypt_encryption_service'
