require 'bcrypt'

class BcryptEncryptionService
  def encrypt(value)
    BCrypt::Password.create(value)
  end
end

EncryptionService.register :bcrypt, BcryptEncryptionService.new
