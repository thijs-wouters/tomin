class FakeEncryptionService
  def encrypt(value)
    value
  end
end

EncryptionService.register(:fake, FakeEncryptionService.new)
